package com.filez.isipdb;

import java.io.IOException;

import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;

public class init {

	public static EmbeddedCacheManager manager;
	public static final int RECORDS = 1000000;
	public static final String CACHE = "sipdb2";

	public static void main(String[] args) throws IOException {
		manager = new DefaultCacheManager("com/filez/sipdb/infinispan.xml");
		StopWatch clock = new StopWatch();
		manager.getCache(CACHE);
		clock.start();
		fillCache(RECORDS);
		System.out.print(RECORDS+" cache filled in ");
		System.out.print(DurationFormatUtils.formatDurationHMS(clock.getTime()));
		manager.stop();
	}

	static void fillCache(int records) {
		Cache <Integer, String> cache = manager.getCache(CACHE);
		cache.clear();
		for (int i=1;i<=records;i++) {
			cache.put(i, i+"@customer" + RandomStringUtils.randomNumeric(3) + ".sip.ourdomain.net");
		}
	}
}
